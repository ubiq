CmdUtils.CreateCommand({
  name: "prevedi",
  description: "Prevodi sa srpskog na engleski i obrnuto",
  help: "Ukucaj nesto i pazi na prevod",
  icon: "http://metak.com/favicon.ico",
  author: {
    name: "Marko Kocic",
    email: "marko.kocic@gmail.com"
  },
  version: "0.1",
  license: "LGPL",

  takes: {
    "rec" : noun_arb_text
  },

  preview: function(pblock, data) {
    jQuery.ajax({
      type: "post",
      url:"http://metak.com/index.php/recnik/search",
      data: {
        word: escape(data.text)
      },
      dataType:"html",
      error:function() {
        pblock.innerHTML="Greska prilikom prevodjenja";
      },

      success: function(result) {
        pblock.innerHTML = result;
      }
    });
  },

  execute: function() {
    Utils.openUrlInBrowser("http://metak.com");
  }
});
