CmdUtils.CreateCommand({
  name: "java",
  homepage: "http://www.cse.ucsd.edu/users/jnguy/java.html",
  description: "Search's java 1.6 API",
  help: "Simply type java and the classname and it will do a google search for API documentation on that class",
  icon: "http://www.sun.com/favicon.ico",
  takes: {"class name": noun_arb_text},

  preview: function(pblock, directObject) {
    var searchTerm = directObject.text;
    var pTemplate = "Searches Java for <b>${query}</b>";
    var pData = {query: searchTerm};
    pblock.innerHTML = CmdUtils.renderTemplate(pTemplate, pData);
    var url = "http://ajax.googleapis.com/ajax/services/search/web";
    var params = {
      v: "1.0",
      q: "site:http://java.sun.com/javase/6/docs/api/ " + searchTerm
    };
    jQuery.get(url, params, function(data) {
		 var numToDisplay = 3;
		 var results = data.responseData.results.splice( 0, numToDisplay );
                 pblock.innerHTML = CmdUtils.renderTemplate(
                   {file: "google-search.html"},
                   {results: results}
		 );
               }, "json");
  },

  execute: function(theClass) {
    var URL = "http://www.google.com/search?hl=en&q=site:http://java.sun.com/javase/6/docs/api/%20"
      + theClass.text
      + "&btnG=Search&btnI=3564";
    Utils.openUrlInBrowser(URL);
  }
});
