CmdUtils.makeSearchCommand({
  name: "gmane",
  url: "http://gmane.org/find.php?list={QUERY}",
  icon: "http://gmane.org/favicon.ico",
  description: "Go to GMANE to look up given list",
  author: {
    name: "Marko Kocic",
    email: "marko.kocic@gmail.com"
  },
  license: "LGPL",
  version: "0.1"
});
