CmdUtils.CreateCommand({
  name: "mail",
  icon: "http://mail.google.com/mail/images/favicon.ico",
  execute: function() {
    Utils.openUrlInBrowser("http://mail.google.com/");
  },
  preview: function( pblock ) {
    pblock.innerHTML = "Checking gmail ..."
    var url = "https://mail.google.com/mail/feed/atom/Unread";

    jQuery.get( url, {}, function(feedXml){
      var entries = jQuery("entry", feedXml);
      var text = "No new mail.";

      if (entries.length > 0) {
        text = "";
        entries.each(function(i) {
          var entry = jQuery(this);
          var link = entry.find("link").attr("href");
          var title = entry.find("title").text();
          if (title == "") title = "(No subject)";
          var author = entry.find("author name").text();
          var summary = entry.find("summary").text();

          text += "<a href='" + link  + "'>" + title  + "</a><br/>";
          text += "From: " + author + "<br/>";
          text += summary + "<br/><br/>";
        });
      }

      pblock.innerHTML = text ;
    });
  }
});
