(function() {

  var noun_type_grad = new CmdUtils.NounType (
    "ime mesta",
    ["Beograd", "Cacak", "Valjevo", "Kragujevac"]
  );
  
  CmdUtils.CreateCommand({
    name: "adresa",
    homepage: "http://v0xel.ubuntusrbija.org",
    description: "Pretražuje B92 mape za zadatu ulicu i grad",
    author: {
  	name: "Petar Vasic",
  	email: "p.vasic@gmail.com"
    },
    contributors: [
  	"Raca Mirko",
  	"Marko Kocic <marko.kocic@gmail.com>"
    ],
    licence: "MPL",
    help: "Pretražuje mape.b92.net za zadatu ulicu i broj, u zadatom gradu (podrazumevani grad je Beograd)",
  
    takes: {"ulica": noun_arb_text},
    modifiers: {
  	'broj': noun_arb_text,
  	'mesto': noun_type_grad
    },
  
    preview: function (pblock, street, mods) {
  	pblock.innerHTML = "Korišćenje: <em>ime_ulice</em> broj <em>broj</em> mesto <em>ime_mesta</em>";
    },
  
    execute: function( street, mods ) {
  	var url = "http://mape.b92.net/map.aspx";
  	url += "?ulica=" + escape(street.text);
  	if (mods.broj.text !== undefined) {
  	  url += "&Broj=" + escape(mods.broj.text);
  	}
  	if (mods.mesto.text === undefined) {
  	  url += "&mesto=Beograd";
  	} else {
  	  url += "&mesto=" + escape(mods.mesto.text);
  	}
  	Utils.openUrlInBrowser(url);
    }
  });
})();

